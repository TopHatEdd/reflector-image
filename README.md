# Reflector container image
Builds a [Reflector](https://xyne.dev/projects/reflector/index.html) 
container image that runs reflector at intervals. To be used
as a companion for [Pacoloco](https://github.com/anatol/pacoloco) and refresh
the mirror list it uses, at intervals.  

## Environment vars
* SLEEP_DURATION - Interval, in seconds, between reflector invocation.
  - Default to 24 hours.
* HEALTHCHECK_FILE - File path to last execution's exit code for container
healthcheck.
  - Default to /tmp/lastexit

## Build
We build for both armv7 and amd64. The former is the deployed server's
architecture.
```bash
docker buildx \
	build \
	--pull \
	--no-cache \
	--platform linux/amd64,linux/arm/v7 \
	--push \
	-t tophatedd/reflector:2021.11 \
	.
```
> We want the latest base image more than using the cache as code changes are
less frequent than upstream's image fixes..

### BuiltKit setup
[Docker docs](https://docs.docker.com/buildx/working-with-buildx/#build-multi-platform-images)
```
QEMU is the easiest way to get started.
For QEMU binaries registered with binfmt_misc on the host OS to work transparently inside containers, they must be statically compiled and registered with the fix_binary flag. 
```

[Package manager managed solution](https://aur.archlinux.org/packages/binfmt-qemu-static-all-arch#comment-834965)
```bash
yay -S aur/qemu-user-static-bin
yay -S aur/binfmt-qemu-static-all-arch
```

`error: multiple platforms feature is currently not supported for docker driver. Please switch to a different driver (eg. "docker buildx create --use")`
```bash
docker buildx create --use
```

## Usage examples
Standalone container
```bash
docker run \
	--rm \
	--mount type=tmpfs,destination=/.cache \
	-v $(pwd):/var/cache/reflector \
	tophatedd/reflector:2021.11 \
		--protocol https \
		--country Finland,France,Germany,Greece,Norway,Romania,Sweden \
		--latest 5 \
		--sort rate \
		--save /var/cache/reflector/mirrors          
```
[docker-compose][docker-compose.yml]
```yaml
version: "2.4"
services:
  reflector:
    image: tophatedd/reflector:2021.11
    restart: "unless-stopped"
    volumes:
      - type: tmpfs
        target: /.cache 
      - type: bind
        source: /path/to/whatever/requires
        target: /var/cache/reflector
    command:
      - "--protocol"
      - "https"
      - "--country"
      - "Finland,France,Germany,Greece,Norway,Romania,Sweden"
      - "--latest"
      - "5"
      - "--sort"
      - "rate"
      - "--save"
      - "/var/cache/reflector/mirrors"
```
> 2.4 was the highest version that satisfied all requirements on the server.
> Don't remember what they were.
