FROM python:3.10-slim

RUN pip install --no-cache-dir https://xyne.dev/projects/reflector/src/reflector-2021.11.tar.xz
USER 1000:1000
COPY ./reflect /usr/local/bin/reflect
ENV HEALTHCHECK_FILE=/tmp/lastexit
ENV SLEEP_DURATION=86400

HEALTHCHECK CMD grep 0 "$HEALTHCHECK_FILE"

ENTRYPOINT ["/usr/local/bin/reflect"]
